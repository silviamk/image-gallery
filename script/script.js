$(function() {
    const nowY = new Date().getFullYear();
    for(let Y=nowY; Y>=1900; Y--) {
        $('#yearOfBirth').append(`<option value="${Y}">${Y}</option>`); 
    }

});



//Get the people list on load
const url                   = 'https://randomuser.me/api/?results=200';//https://cors-anywhere.herokuapp.com/
let people                  = '';
const pagination_element    = $('#pagination');
let current_page            = 1;
let rows                    = 16;


function getData() {
    return new Promise((resolve) => {
        fetch(url, {
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then(response => {
                return response.json();
            }).then(data_from_fetched => {
            let data = data_from_fetched;
            resolve(data);
        })
    })
}

function displayPeople(people){
    $.each(people, function (index, element) {
        let anc = $("<a href='#' class='single-image'></a>");
        $(anc).prop('id', 'people-index' + index);
        $('#person-img').append(anc);
        let img = $("<img class='image-person'>");
        $(img).attr("src", element.picture.large);
        $(anc).append(img);
        let figcaption = $("<figcaption class='green-color text-style'>");
        $(figcaption).text(element.name.first + " " + element.name.last);
        $(anc).append(figcaption);
    });
}

$("#person-img").on( "click", ".single-image", function() {

    let index = $(this).index();
    localStorage.setItem('person', JSON.stringify(people.results[index]));
    window.open("personal-view.html", "_self");
});
// if no people save in localStorage fetch fresh copy from api and save it in the localStorage
if (localStorage.getItem('people') === null) {
    getData().then(data => {
        people = data;
        if (!people || !people.results) {
            return;
        }
        people.results.sort((a, b) => {
            //a may be null or undefined, a.name as well
            let aVal = a && a.name ? a.name.first : "";
            let bVal = b && b.name ? b.name.first : "";
            return aVal > bVal ? 1 : -1;

        });
        // debugger;
        // Store
        localStorage.setItem('people', JSON.stringify(people));
        if(people){

           SetupPagination(people.results, pagination_element, rows);
           DisplayList(people.results, rows, current_page);
        }

    });
} else { // if people found in localStorage do not fetch again from api, just display them
    console.log('getting existing data')
    people = JSON.parse(localStorage.getItem("people"));
    if(people){

        SetupPagination(people.results, pagination_element, rows);
        DisplayList(people.results, rows, current_page);
    }
    console.log(people);
}


function DisplayList (items, rows_per_page, page) {

    $('#person-img').empty();
	page--;

	let start = rows_per_page * page;
	let end = start + rows_per_page;
	let paginatedItems = items.slice(start, end);

    displayPeople(paginatedItems);

}
//Pagination
function SetupPagination (items, wrapper, rows_per_page) {
    wrapper.innerHTML = "";

    let page_count = Math.ceil(items.length / rows_per_page);
    for (let i = 1; i < page_count + 1; i++) {
        let btn = PaginationButton(i, items);
        wrapper.append(btn);
    }
}

function PaginationButton (page) {
    let button = $("<button></button>");

    $(button).text(page);

    if (current_page == page){
        $(button).addClass('active');
    }
    $(button).on( "click", function() {
    
        current_page = page;
        DisplayList(people.results, rows, current_page);
        let current_btn = $('.pagenumbers').find('button.active');//document.querySelector('.pagenumbers button.active');//$('.pagenumbers button.active');
        
        current_btn.removeClass('active');

        $(button).addClass('active');
    });
    return button;
}

//Allow filtering on personal list. Add search option, that searches over personal name. 
let fnSearch = function () {

    let filter = $("#myInput").val().toUpperCase();
    let txtValue;
    let searchedName = people.results;

    for (let i = 0; i < searchedName.length; i++) {

        txtValue = searchedName[i].name.first + " " + searchedName[i].name.last;
        
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            $('#people-index' + i).show();
        } else {
            $('#people-index' + i).hide();
        }
    }
}
let fnFilterBirthYear = function() {
    $.each(people.results, function (index, element) {
        let birthYear = element.dob.date.substr(0,4);
        let selectedYear = $('#yearOfBirth').val();
        if(birthYear == selectedYear) {
            $('#people-index' + index).show();
        } else {
            $('#people-index' + index).hide(); 
        }
    });
}
