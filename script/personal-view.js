let personObj = JSON.parse(localStorage.getItem("person"));
console.log(personObj);

let img = $("<img>");
$(img).attr("src", personObj.picture.large);
$("#personPhoto").append(img);
var fullNameWrap = $("<div id='fullNameWrap'></div>");
var fullNameTitle = $("<label class='title'>Full name:</label>");
var fullName = $("<input disabled type='text' name= 'fullName'/>");
$(fullName).val(personObj.name.first + " " + personObj.name.last);
$("#personalDetails").append(fullNameWrap);
$("#fullNameWrap").append(fullNameTitle);
$("#fullNameWrap").append(fullName);

var emailWrap = $("<div id='emailWrap'></div>");
var emailTitle = $("<label class='title'>E-mail:</label>");
var email = $("<input disabled type='text' name= 'email'/>");
$(email).val(personObj.email);
$("#personalDetails").append(emailWrap);
$("#emailWrap").append(emailTitle);
$("#emailWrap").append(email);

var phoneWrap = $("<div id='phoneWrap'></div>");
var phoneTitle = $("<label class='title'>Phone:</label>");
var phone = $("<input disabled type='text' name= 'phone'/>");
$(phone).val(personObj.phone);
$("#personalDetails").append(phoneWrap);
$("#phoneWrap").append(phoneTitle);
$("#phoneWrap").append(phone);

var cityWrap = $("<div id='cityWrap'></div>");
var cityTitle = $("<label class='title'>City:</label>");
var city = $("<input disabled type='text' name= 'city'/>");
$(city).val(personObj.location.city);
$("#personalDetails").append(cityWrap);
$("#cityWrap").append(cityTitle);
$("#cityWrap").append(city);

var countryWrap = $("<div id='countryWrap'></div>");
var countryTitle = $("<label class='title'>Country:</label>");
var country = $("<input disabled type='text' name= 'country'/>");
$(country).val(personObj.location.country);
$("#personalDetails").append(countryWrap);
$("#countryWrap").append(countryTitle);
$("#countryWrap").append(country);

var streetWrap = $("<div id='streetWrap'></div>");
var streetNameTitle = $("<label class='title'>Street:</label>");
var streetName = $("<input disabled type='text' name= 'street'/>");
$(country).val(personObj.location.street.name);
$("#personalDetails").append(streetWrap);
$("#streetWrap").append(streetNameTitle);
$("#streetWrap").append(streetName);


var editBtn = $("<button type='button' id='editBtn'>Edit</button>");
$("#btnWrap").append(editBtn);
var saveBtn = $("<button type='submit'>Save</button>");
$("#btnWrap").append(saveBtn);

$("#editBtn").on('click', function(){
    $("input").removeAttr("disabled");
});

